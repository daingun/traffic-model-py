# Biham-Middleton-Levine Traffic Model

https://en.wikipedia.org/wiki/Biham%E2%80%93Middleton%E2%80%93Levine_traffic_model

https://www.jasondavies.com/bml/

## Usage - traffic.py
```
usage: traffic.py [-h] [-o]

Biham-Middleton-Levine model.

optional arguments:
  -h, --help  show this help message and exit
  -o          output to files
```

The program reads an initial configuration from a file named **problem.csv** (the configuration describes the position of red and blue cars in a matrix). This configuration represents step 0.

The first line of the file contains a list of numbers, separated with a comma. This numbers are the steps of the simulation for which the program must write an output (one output for each of the steps in the list).

The remaining lines describe the initial matrix for the simulation (step 0).
Each line describes one row of the matrix. Each line contains a list of numbers, separated with a comma. Numbers can be:
- 0: empty cell
- 1: blue car
- 2: red car

If requested the output is redirected to files. The name of the output files will be <step_number>.csv.

Example of input file:
```
    1,10,100
    0,1,0,1,0
    2,0,2,0,0
    0,0,1,0,0
```

Example of ouput file at step one ("1.cvs"):
```
    0,0,1,0,0
    2,1,2,1,0
    0,0,0,0,0
```

## Usage - traffic_nc.py
```
usage: traffic_nc.py
```

The program shows an animated version of the model using ncurses.
