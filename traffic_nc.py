#!/usr/bin/env python3
"""Curses implementation of the Biham-Middleton-Levine model."""

import curses
import time
import traffic

def setup_model():
    """Set up the traffic model."""
    steps, cars = traffic.import_file()
    return traffic.Traffic(cars), steps[-1]

def setup_curses(stdscr):
    """Set up curses."""
    # Hide the cursor.
    curses.curs_set(0)
    # Set colours and character for the cars.
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    # Set getch() no blocking.
    stdscr.nodelay(True)
    # Clear screen.
    stdscr.clear()

def main(stdscr):
    """Main loop for curses."""
    matrix, max_steps = setup_model()

    setup_curses(stdscr)
    colour_blue = curses.color_pair(1)
    colour_red = curses.color_pair(2)
    char = curses.ACS_BLOCK

    # Initialize outer box.
    height = matrix.rows
    width = matrix.columns
    outer_box = curses.newwin(height + 2, width + 3, 0, 0)
    outer_box.box()
    # Initialize the grid.
    # It must be width+1 otherwise it crashes.
    grid = outer_box.subwin(height, width + 1, 1, 1)
    # Initialize step counter.
    step_counter = curses.newwin(3, curses.COLS, curses.LINES - 3, 0)
    step_counter.box()

    for step in range(1, max_steps + 1):
        c = stdscr.getch()
        curses.flushinp() # Flush any multiple pressed keys.
        if c == ord('q'):
            return
        grid.clear()
        matrix.advance(step)
        for car in matrix.blue_cars:
            grid.addch(car[0], car[1], char, colour_blue)
        for car in matrix.red_cars:
            grid.addch(car[0], car[1], char, colour_red)
        step_counter.addstr(1, 1, "Step: {}".format(step))
        # First refresh the outer windows.
        stdscr.noutrefresh()
        outer_box.noutrefresh()
        grid.noutrefresh()
        step_counter.noutrefresh()
        # Update the screen
        curses.doupdate()
        time.sleep(0.5)

    stdscr.nodelay(False) # Set getkey() blocking.
    stdscr.getkey()

if __name__ == '__main__':
    curses.wrapper(main)
