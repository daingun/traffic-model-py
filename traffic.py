#!/usr/bin/env python3
"""Biham-Middleton-Levine model."""

import argparse
import csv
from os import mkdir, path

class Traffic:
    """Grid containing car data."""

    def __init__(self, cars):
        """Initialize the object representin the car grid."""
        self.blue_cars, self.red_cars = self.store_cars(cars)
        self.rows, self.columns = self.get_dimensions(cars)

    def main_loop(self, steps, output):
        """Main steps of the iterations."""
        total = steps[-1]
        for step in range(1, total + 1):
            self.advance(step)
            if step in steps:
                self.print_grid(step)
                if output:
                    self.output_to_file(step)

    def advance(self, step):
        """Move the cars."""
        if step % 2 is 1:
            self.blue_cars = self.push_down(self.blue_cars)
        else:
            self.red_cars = self.push_right(self.red_cars)

    def to_string(self):
        """Create the output structure for console or file"""
        output = []
        for row in range(self.rows):
            line = []
            for column in range(self.columns):
                if (row, column) in self.blue_cars:
                    line.append("1")
                elif (row, column) in self.red_cars:
                    line.append("2")
                else:
                    line.append("0")
            output.append(line)
        return output

    def print_grid(self, step):
        """Print out the grid."""
        print("Step:", step)
        for line in self.to_string():
            print(",".join(line))

    def output_to_file(self, step):
        """Output to a file called {step}.csv"""
        if not path.exists("output/"):
            mkdir("output/")
        with open("output/{0}.csv".format(step), 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerows(self.to_string())

    def push_blu_car(self, blue_car):
        """Move down the blue cars, using modulo for wrap."""
        row, column = blue_car
        return (row + 1) % self.rows, column

    def push_red_car(self, red_car):
        """Move right the red cars, using modulo for wrap."""
        row, column = red_car
        return row, (column + 1) % self.columns

    def push_down(self, car_list):
        """Apply movement to all blue cars.
        The function returns a new list so the input is not modified."""
        def conditional_push_down(car):
            """Return the next position of the car
            checking if the movement is allowed.
            """
            new_car = self.push_blu_car(car)
            if new_car in self.blue_cars or new_car in self.red_cars:
                return car
            else:
                return new_car
        return {conditional_push_down(car) for car in car_list}

    def push_right(self, car_list):
        """Apply movement to all red cars.
        The function returns a new list so the input is not modified."""
        def conditional_push_right(car):
            """Return the next position of the car
            checking if the movement is allowed.
            """
            new_car = self.push_red_car(car)
            if new_car in self.red_cars or new_car in self.blue_cars:
                return car
            else:
                return new_car
        return {conditional_push_right(car) for car in car_list}

    @staticmethod
    def get_dimensions(cars):
        """Get the number of rows and columns from the cars"""
        rows = len(cars)
        columns = len(cars[0])
        return rows, columns

    @staticmethod
    def store_cars(cars):
        """Store the cars in the cars in two sets of tuples."""
        blue_cars = set()
        red_cars = set()
        for (row, line) in enumerate(cars):
            for (column, car) in enumerate(line):
                if car == "1":
                    blue_cars.add((row, column))
                elif car == "2":
                    red_cars.add((row, column))
        return blue_cars, red_cars


def import_file():
    """Import the data from the file."""
    data = []
    with open("problem.csv", "r", newline="") as csvfile:
        file_reader = csv.reader(csvfile)
        for line in file_reader:
            data.append(line)
    return [int(i) for i in data[0]], data[1:]


def main():
    """Main function."""
    parser = argparse.ArgumentParser(description="Biham-Middleton-Levine model.")
    parser.add_argument("-o", action="store_true", help="output to files")
    args = parser.parse_args()

    steps, cars = import_file()
    print(steps, cars)
    matrix = Traffic(cars)
    matrix.print_grid(0)
    matrix.main_loop(steps, args.o)

if __name__ == '__main__':
    main()
